"""crm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import sys

from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path
from django.views.generic import TemplateView

from landing.views import StaticViewSitemap


sitemaps = {
    'static': StaticViewSitemap,
}

urlpatterns = [
    path('_admin_r0b0/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('subscribes/', include('subscribe.urls')),
    path('', include('landing.urls')),
    path('polls/', include('poll.urls')),
    path('blog/', include('blog.urls')),
    path('kursy/', include('tariff.urls')),
    path('organization/', include('organization.urls')),

    path('private/', include('private.urls')),
    path('payments/', include('payment.urls')),
    path('raspisanie/', include('timetable.urls')),
    path('dashboard/', include('dashboard.urls')),

    path('privacy/', TemplateView.as_view(
        template_name='landing/privacy.html'), name='privacy'),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap'),
    path('api/', include(('api.urls', 'api'), namespace='api')),
    path('api-auth/',
         include('rest_framework.urls', namespace='rest_framework')),

    path(r'tz_detect/', include('tz_detect.urls')),

]


TESTING_MODE = 'test' in sys.argv

if settings.DEBUG or TESTING_MODE:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
