import logging
import time
from .celery import app

logger = logging.getLogger(__name__)


class SomeTask(app.Task):

    def __init__(self):
        self.max_retries = 3
        self.time_limit = 30
        self.default_retry_delay = 10

    def _run(self, *args, **kwargs):
        logging.debug(f'Run task: {self}')
        time.sleep(10)
        return args, kwargs

    def run(self, *args, **kwargs):
        try:
            return self._run(*args, **kwargs)
        except Exception as e:
            logger.exception('Got unhandled exception')
            self.retry()


some_task = app.register_task(SomeTask())
