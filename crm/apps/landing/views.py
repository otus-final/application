from django.contrib.sitemaps import Sitemap
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy

from blog.models import Article
from organization.models import Organization
from poll.models import Poll


class IndexView(TemplateView):
    template_name = 'landing/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articles'] = Article.objects.all()
        return context


class OrganizationViewMixin(TemplateView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['organization'] = Organization.objects.first()
        return context


class ContactsView(OrganizationViewMixin):
    template_name = 'landing/contacts.html'


class StaticViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'
    protocol = 'https'

    def items(self):
        items = [
            ('landing:index', None),
            ('landing:contacts', None),
            ('tariff:tariff_list', None),
            ('tariff:course_list', None),
            ('organization:instructor_list', None),
            ('blog:list', {'article_type': Article.NEWS}),
            ('blog:list', {'article_type': Article.ARTICLES}),
        ]
        try:
            # запись на пробное занятие
            poll = Poll.objects.get(slug='zapis-na-probnoe-zanyatie')
            items.append(('poll:detail', {'slug': poll.slug}))
        except Poll.DoesNotExist:
            pass

        return items

    def location(self, item):
        return reverse_lazy(item[0], kwargs=item[1])
