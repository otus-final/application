import datetime

from django.conf import settings
from django.db import transaction
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views import View
from django.views.generic.list import ListView

from utils.sendgrid.client import send_message
from .models import Poll, Question, Answer
from .forms import poll_factory_form, POLL_FIELD_PREFIX


class UniqueAnswerAlreadyExists(Exception):
    pass


class PollView(View):
    template_name = 'poll/detail.html'

    def get(self, request, slug, *args, **kwargs):
        if hasattr(request, 'session') and not request.session.session_key:
            request.session.save()
            request.session.modified = True

        poll = get_object_or_404(Poll, slug=slug)
        form = poll_factory_form(poll)
        return render(
            request,
            self.template_name,
            {'poll': poll, 'form': form}
        )

    @staticmethod
    def _create_answer_obj(question_id: int,
                           answer: str,
                           session: str) -> Answer:
        # todo отрефакторить - сделать проверку одним
        #  запросом а не для каждого ответа по запросу
        question = Question.objects.get(id=question_id)
        if question.is_unique and Answer.objects.filter(
                question=question, answer_text=answer).exists():
            raise UniqueAnswerAlreadyExists
        if answer:
            return Answer(
                session=session,
                question=question,
                answer_text=answer
            )

    @staticmethod
    def send_answer_to_email(title: str, email: str, data: dict):
        dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        subject = f'Ответ на "{title}" от {dt}'
        message = render_to_string(
            'poll/email_msg.html',
            {'data': data}
        )
        send_message(settings.DEFAULT_FROM_EMAIL, [email], subject, message)

    def post(self, request, slug, *args, **kwargs):
        poll = get_object_or_404(Poll, slug=slug)
        form = poll_factory_form(poll)
        session_key = request.session.session_key

        form = form(request.POST)
        error = None
        email_data, send_email = {'answers': []}, True
        if form.is_valid():
            try:
                with transaction.atomic():
                    answer_objects = []
                    for k, v in form.cleaned_data.items():
                        email_data['answers'].append(v)
                        if k.startswith(POLL_FIELD_PREFIX):
                            question_id = int(k.split('-')[1])
                            answer_obj = self._create_answer_obj(
                                question_id, v, session_key)
                            if answer_obj:
                                answer_objects.append(answer_obj)
                    Answer.objects.bulk_create(answer_objects)
            except UniqueAnswerAlreadyExists:
                error = 'Вы уже оставляли заявку'
                send_email = False

            if poll.email and send_email:
                email_data['user'] = request.user
                email_data['session_key'] = session_key
                self.send_answer_to_email(poll.title, poll.email, email_data)

            return render(request, 'poll/success.html', {'error': error})

        return render(request, self.template_name, {'poll': poll, 'form': form})


class PollListView(ListView):
    model = Poll
    template_name = 'poll/list.html'
