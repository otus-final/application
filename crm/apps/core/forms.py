from django.contrib.auth.forms import UserCreationForm
from .models import User


class CustomSignupForm(UserCreationForm):

    class Meta:
        fields = (
            'username', 'email', 'last_name', 'first_name',
            'password1', 'password2')
        model = User
