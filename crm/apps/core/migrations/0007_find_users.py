from django.contrib.auth import get_user_model
from django.db import migrations, models


def update_user_as_student(apps, schema_editor):
    # User = Offer = apps.get_model('core', 'User')
    User = get_user_model()
    User.objects.filter(comment__icontains='ученик').update(is_student=True)


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_user_is_student'),
    ]

    operations = [
        migrations.RunPython(update_user_as_student,
                             reverse_code=migrations.RunPython.noop)
    ]
