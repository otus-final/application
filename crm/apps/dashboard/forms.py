from django import forms


class PhotoFromLessonForm(forms.Form):
    photos = forms.ImageField(
        label='Фотографии',
        widget=forms.ClearableFileInput(attrs={'multiple': True})
    )


class LessonListFilterForm(forms.Form):
    date_start = forms.DateField(label='C даты')
    date_end = forms.DateField(label='По дату')
    course = forms.ChoiceField(label='Курс')
