from django.urls import path
from . import views

app_name = 'dashboard'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('payments', views.PaymentStatView.as_view(), name='payment_stat'),
    path('lessons', views.LessonListView.as_view(), name='lesson_list'),
    path('lessons/<pk>', views.LessonDetailView.as_view(),
         name='lesson_detail'),
]
