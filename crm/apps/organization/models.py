from ckeditor.fields import RichTextField
from django.db import models

from django.contrib.auth import get_user_model

User = get_user_model()


class Organization(models.Model):
    managers = models.ManyToManyField(User, blank=True,
                                      verbose_name='Руководители')
    name = models.CharField('Название', max_length=200, )
    full_name = models.CharField('Полное название', max_length=255)
    domain = models.SlugField('Поддомен')
    address = models.CharField('Адрес', max_length=255)
    map_link = models.TextField('Ссылка на карту', blank=True, null=True)
    contacts = RichTextField('Контакты')
    requisites = RichTextField('Рекивзиты')
    offer_agreement_url = models.URLField(
        'Ссылка на договор оферту', blank=True)
    sb_username_api = models.CharField(
        'API sberbank username ', max_length=200, blank=True,
        help_text='Имя пользователя API сбербанк эквайринга')
    sb_password_api = models.CharField(
        'API sberbank password ', max_length=200, blank=True,
        help_text='Пароль пользователя API сбербанк эквайринга')
    sb_url_api = models.URLField(
        'API sberbank rest url',
        default='https://3dsec.sberbank.ru/payment/rest/',
        help_text='URL rest API сбербанк эквайринга'
    )
    sb_demo_mode = models.BooleanField(
        'API sberbank demo mode', default=True, blank=True,
        help_text='Включить демонстрационный режим API сбербанк эквайринга')
    created = models.DateTimeField('Дата создания', auto_now_add=True)
    updated = models.DateTimeField('Дата изменения', auto_now=True)

    class Meta:
        verbose_name = 'Организации'
        verbose_name_plural = 'Организации'

    def __str__(self):
        return self.name


class Instructor(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    user = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.SET_NULL,
        verbose_name='Пользователь')
    about = RichTextField('О преподавателе')
    sorting_number = models.PositiveIntegerField('Номер по порядку', default=1)
    updated = models.DateTimeField('Дата изменения', auto_now=True)

    class Meta:
        verbose_name = 'Преподаватель'
        verbose_name_plural = 'Преподаватели'
        ordering = ('sorting_number',)

    def __str__(self):
        return self.user.get_full_name()
