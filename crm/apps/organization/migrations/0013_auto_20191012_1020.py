# Generated by Django 2.2.5 on 2019-10-12 05:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20191007_1059'),
        ('organization', '0012_auto_20191012_1017'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organization',
            name='user',
        ),
        migrations.AddField(
            model_name='organization',
            name='managers',
            field=models.ManyToManyField(blank=True, to='core.User', verbose_name='Руководители'),
        ),
    ]
