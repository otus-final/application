from django.db import models


class Subscribe(models.Model):
    email = models.EmailField(verbose_name='Email', unique=True)
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'

    def __str__(self):
        return self.email
