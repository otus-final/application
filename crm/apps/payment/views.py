from django.contrib import messages

from django.shortcuts import HttpResponseRedirect
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from payment.mixins import PaidSeasonTicketMixin


class PaymentSuccess(TemplateView, PaidSeasonTicketMixin):
    template_name = 'payment/success.html'

    def get(self, request, *args, **kwargs):
        acquiring_order_id = request.GET.get('orderId')

        if acquiring_order_id:
            resp, error = self.get_paid_result(acquiring_order_id)
            if not error:
                messages.add_message(request, messages.SUCCESS,
                                     'Успешная оплата')
            else:
                messages.add_message(request, messages.WARNING,
                                     'Ошибка оплаты')
        return HttpResponseRedirect(reverse_lazy('private:index'))
