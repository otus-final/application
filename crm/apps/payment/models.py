from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.db import models

from organization.models import Organization

User = get_user_model()


class Payment(models.Model):
    CASH = 'cash'
    SB = 'sb_aq'

    PAYMENT_TYPES = (
        (CASH, 'Наличными'),
        (SB, 'Сбербанк эквайринг'),
    )

    CREATED = 'created'
    PENDING = 'pending'
    PAID = 'paid'
    ERROR = 'error'

    PAYMENT_STATUSES = (
        (CREATED, 'Создан'),
        (PENDING, 'Ожидание оплаты'),
        (PAID, 'Оплачен'),
        (ERROR, 'Ошибка'),
    )

    organization = models.ForeignKey(
        Organization, verbose_name='Организация', on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                             blank=True)
    amount = models.DecimalField(
        'Сумма', max_digits=9, decimal_places=2, default='0.0')

    payment_type = models.CharField('Тип оплаты', max_length=5, default=SB,
                                    choices=PAYMENT_TYPES)
    status = models.CharField(
        'Статус', max_length=10, choices=PAYMENT_STATUSES,
        default=CREATED)
    acquiring_order_id = models.CharField(
        'Идентификатор платежа в эквайринге', max_length=36, blank=True,
        db_index=True)
    info = JSONField(
        'Служебная информация', default=dict, blank=True, null=True)
    comment = models.CharField('Комментарий', max_length=255, blank=True)
    created = models.DateTimeField('Дата создания', auto_now_add=True)
    updated = models.DateTimeField('Дата изменения', auto_now=True)

    class Meta:
        verbose_name = 'Платеж'
        verbose_name_plural = 'Платежи'

    def __str__(self):
        return f'{self.amount}'

