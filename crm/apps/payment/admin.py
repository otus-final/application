from django.contrib import admin
from django.contrib.contenttypes.models import ContentType

from .models import Payment


class PaymentAdmin(admin.ModelAdmin):

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj=None, change=False, **kwargs)
        form.base_fields['content_type'].queryset = (
            ContentType.objects.filter(model__in=('seasonticket',)))
        return form

    readonly_fields = ('created', 'updated')
    list_filter = ('payment_type', 'created', 'status', 'content_type', )
    list_display = ('id', 'user', 'amount', 'payment_type', 'created',
                    'status', 'content_type',
                    'object_id')
    raw_id_fields = ('user',)


admin.site.register(Payment, PaymentAdmin)
