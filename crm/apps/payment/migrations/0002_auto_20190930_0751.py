# Generated by Django 2.2.5 on 2019-09-30 07:51

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0011_auto_20190928_0648'),
        ('payment', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='acquiring_order_id',
            field=models.CharField(blank=True, max_length=36, verbose_name='Идентификатор платежа в эквайринге'),
        ),
        migrations.AddField(
            model_name='payment',
            name='info',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=dict, null=True, verbose_name='Служебная информация'),
        ),
        migrations.AddField(
            model_name='payment',
            name='organization',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='organization.Organization', verbose_name='Организация'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='payment',
            name='payment_type',
            field=models.CharField(default='sb_aq', max_length=5, verbose_name='Тип оплаты'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='description',
            field=models.CharField(blank=True, max_length=255, verbose_name='Описание'),
        ),
    ]
