import logging
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import ListView, TemplateView

from organization.models import Organization
from payment.mixins import PaidSeasonTicketMixin

from .forms import SeasonTicketOrderForm
from .models import Course, Tariff, SeasonTicket


logger = logging.getLogger('crm')


class ListViewHeadMixin(ListView):

    def head(self, *args, **kwargs):
        last_tariff = self.get_queryset().latest('updated')
        response = HttpResponse('')
        # RFC 1123 date format
        response['Last-Modified'] = last_tariff.updated.strftime(
            '%a, %d %b %Y %H:%M:%S GMT')
        return response


class TariffListView(ListViewHeadMixin):
    template_name = 'tariff/tariff_list.html'
    context_object_name = 'tariffs'
    queryset = Tariff.objects.filter(cost__gt=0).order_by('course', 'cost')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['organization'] = Organization.objects.first()
        return context


class CourseListView(ListViewHeadMixin):
    template_name = 'tariff/course_list.html'
    context_object_name = 'courses'
    queryset = Course.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['organization'] = Organization.objects.first()
        return context


class SeasonTicketCreateView(PaidSeasonTicketMixin, TemplateView):
    template_name = 'tariff/season_ticket_create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tariff'] = get_object_or_404(Tariff, id=kwargs['tariff_pk'],
                                              cost__gt=0)
        context['organization'] = Organization.objects.first()
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['form'] = SeasonTicketOrderForm()
        return self.render_to_response(context)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        tariff = context['tariff']
        org = Organization.objects.first()

        form = SeasonTicketOrderForm(request.POST)
        if form.is_valid() and org.sb_test_mode:
            data = form.cleaned_data
            season_ticket = SeasonTicket.objects.create(
                tariff=tariff,
                buyer=request.user,
                comment=data['comment']
            )
            return self.paid_season_ticket(request, season_ticket,
                                           data['comment'])

        context['form'] = form
        return self.render_to_response(context)
