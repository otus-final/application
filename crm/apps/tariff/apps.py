from django.apps import AppConfig


class TariffConfig(AppConfig):
    name = 'tariff'
    verbose_name = 'Тарифы'

    # def ready(self):
    #     import tariff.signals
