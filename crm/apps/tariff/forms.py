from django import forms
# from django.urls import reverse_lazy


class SeasonTicketOrderForm(forms.Form):
    comment = forms.CharField(
        label='Комментарий:',
        widget=forms.Textarea, max_length=100, required=False)
