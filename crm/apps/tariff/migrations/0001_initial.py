# Generated by Django 2.2.5 on 2019-09-22 10:59

import ckeditor.fields
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('organization', '0008_auto_20190922_0937'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    S_TICKET_APP_LABEL = 'season_ticket'
    S_TICKET_CONTENT_TYPE_ID_SQL = f"SELECT id from django_content_type " \
        f"WHERE app_label='{S_TICKET_APP_LABEL}'"

    operations = [

        migrations.RunSQL(
            "DROP TABLE IF EXISTS season_ticket_seasonticket_confidants;",
            reverse_sql=''
        ),

        migrations.RunSQL(
            "DROP TABLE IF EXISTS season_ticket_seasonticket;",
            reverse_sql=''
        ),

        migrations.RunSQL(
            "DROP TABLE IF EXISTS season_ticket_tariff;",
            reverse_sql=''
        ),

        migrations.RunSQL(
            f"DELETE FROM django_admin_log WHERE content_type_id IN ({S_TICKET_CONTENT_TYPE_ID_SQL});",
            reverse_sql=''
        ),

        migrations.RunSQL(
            f"DELETE FROM auth_permission WHERE content_type_id IN ({S_TICKET_CONTENT_TYPE_ID_SQL});",
            reverse_sql=''
        ),

        migrations.RunSQL(
            f"DELETE FROM django_content_type WHERE app_label='{S_TICKET_APP_LABEL}';",
            reverse_sql=''
        ),

        migrations.RunSQL(
            f"DELETE FROM django_migrations WHERE app='{S_TICKET_APP_LABEL}';",
            reverse_sql=''
        ),

        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120, verbose_name='Название курса')),
                ('description', ckeditor.fields.RichTextField(verbose_name='Описание')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='organization.Organization', verbose_name='Организация')),
            ],
            options={
                'verbose_name': 'Курс',
                'verbose_name_plural': 'Курсы',
            },
        ),
        migrations.CreateModel(
            name='Tariff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
                ('is_active', models.BooleanField(blank=True, default=True)),
                ('cost', models.PositiveIntegerField(verbose_name='Стоимость')),
                ('lesson_count', models.PositiveIntegerField(verbose_name='Количество занятий')),
                ('expiration_date', models.PositiveIntegerField(verbose_name='Срок действия в днях')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлен')),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tariff.Course', verbose_name='Курс')),
            ],
            options={
                'verbose_name': 'Тариф',
                'verbose_name_plural': 'Тарифы',
            },
        ),
        migrations.CreateModel(
            name='SeasonTicket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('expiration_date', models.DateField(blank=True, null=True, verbose_name='Дата окончания')),
                ('code', models.CharField(max_length=10, null=True, unique=True, verbose_name='Код')),
                ('discount', models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Скидка')),
                ('comment', models.TextField(blank=True, null=True, verbose_name='Коментарий')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Дата покупки')),
                ('buyer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='buyer', to=settings.AUTH_USER_MODEL, verbose_name='Покупатель')),
                ('child', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='child', to=settings.AUTH_USER_MODEL, verbose_name='Обучаемый ребенок')),
                ('confidants', models.ManyToManyField(blank=True, help_text='Пользователи могут просматривать данные абонемента', related_name='manager', to=settings.AUTH_USER_MODEL, verbose_name='Доверенные лица покупателя')),
                ('seller', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='seller', to=settings.AUTH_USER_MODEL, verbose_name='Продавец')),
                ('tariff', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tariff.Tariff', verbose_name='Тариф')),
            ],
            options={
                'verbose_name': 'Абонемент',
                'verbose_name_plural': 'Абонементы',
            },
        ),
    ]
