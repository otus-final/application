# Generated by Django 2.2.5 on 2019-09-25 10:01

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0009_organization_offer_agreement'),
        ('tariff', '0002_course_updated'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ['sorting_number'], 'verbose_name': 'Курс', 'verbose_name_plural': 'Курсы'},
        ),
        migrations.RemoveField(
            model_name='course',
            name='organization',
        ),
        migrations.AddField(
            model_name='course',
            name='sorting_number',
            field=models.PositiveIntegerField(default=1, verbose_name='Номер по порядку'),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120, verbose_name='Название курса')),
                ('sorting_number', models.PositiveIntegerField(verbose_name='Номер по порядку')),
                ('description', ckeditor.fields.RichTextField(verbose_name='Описание')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='organization.Organization', verbose_name='Организация')),
            ],
            options={
                'verbose_name': 'Раздел',
                'verbose_name_plural': 'Разделы',
                'ordering': ['sorting_number'],
            },
        ),
        migrations.AddField(
            model_name='course',
            name='section',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='tariff.Section', verbose_name='Раздел'),
        ),
    ]
