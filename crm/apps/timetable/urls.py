from django.urls import path
from . import views

app_name = 'timetable'

urlpatterns = [
    path('', views.TimeTableMonthView.as_view(),
         name='current_month'),
    path('<int:year>-<int:month>', views.TimeTableMonthView.as_view(),
         name='month'),

    path('nedelya',
         views.TimeTableWeekView.as_view(), name='current_week'),
    path('nedelya/<int:year>-<int:month>-<int:day>',
         views.TimeTableWeekView.as_view(), name='week')
]
