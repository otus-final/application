from django.contrib import admin

from .models import Lesson, StudentOnLesson, PhotoFromLesson, LessonContext


class StudentOnLessonInline(admin.TabularInline):
    model = StudentOnLesson
    extra = 1
    raw_id_fields = ('season_ticket', 'lesson')


class PhotoFromLessonInline(admin.TabularInline):
    model = PhotoFromLesson
    extra = 1


class LessonAdmin(admin.ModelAdmin):
    list_display = ('name', 'students_names', 'student_count',
                    'date', 'time_start', 'time_end',
                    'is_completed')
    list_filter = ('instructor', 'time_start', 'time_end', 'date')
    search_fields = ('name',)
    inlines = (StudentOnLessonInline, PhotoFromLessonInline)


admin.site.register(Lesson, LessonAdmin)
admin.site.register(LessonContext)
