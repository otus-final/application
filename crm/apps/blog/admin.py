from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget

from .models import Article


class ArticleAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Article
        fields = '__all__'


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'views_number', 'published', 'updated')
    list_filter = ('article_type', 'organization')


admin.site.register(Article, ArticleAdmin)
