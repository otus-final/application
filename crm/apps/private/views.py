import functools
from django import shortcuts

from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.urls import reverse_lazy

from lesson.models import Lesson
from payment.mixins import PaidSeasonTicketMixin
from tariff.models import SeasonTicket
from tariff.forms import SeasonTicketOrderForm

User = get_user_model()


def check_user_data(f):
    @functools.wraps
    def wrap(request, *args, **kwargs):
        if not all([request.user.last_name, request.user.first_name]):
            return shortcuts.HttpResponseRedirect('/')
        return f(request, *args, **kwargs)
    return wrap


class PrivateIndexView(TemplateView):
    template_name = 'private/index.html'

    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        # return self.render_to_response(self.get_context_data(**kwargs))
        return shortcuts.HttpResponseRedirect(
            shortcuts.reverse('private:season_ticket_list'))


class PrivateLessonListView(ListView):
    template_name = 'private/lessons.html'

    def get_queryset(self):
        queryset = Lesson.objects.filter(
            studentonlesson__season_ticket__buyer=self.request.user)
        return queryset

    @method_decorator(login_required)
    def get(self,  request, *args, **kwargs):
        return super().get(self, request, *args, **kwargs)


class PrivateLessonDetailView(DetailView):
    template_name = 'private/lesson_detail.html'
    model = Lesson
    pk_url_kwarg = 'lesson_pk'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['photos'] = [
            {
                'src': photo.photo.url,
                'h': photo.photo.height,
                'w': photo.photo.width
            }
            for photo in self.object.photofromlesson_set.all()
        ]
        return data


class PrivateSeasonTicketListView(ListView):
    model = User
    template_name = 'private/season_tickets.html'

    def get_queryset(self):
        queryset = SeasonTicket.objects.filter(buyer=self.request.user)
        return queryset

    @method_decorator(login_required)
    def get(self,  request, *args, **kwargs):
        return super().get(self, request, *args, **kwargs)


class SeasonTicketPaidView(PaidSeasonTicketMixin, DetailView):
    model = SeasonTicket
    template_name = 'tariff/season_ticket_paid.html'
    pk_url_kwarg = 'season_ticket_pk'

    def _check_payment_not_possible(self, obj: SeasonTicket):
        if self.request.user != obj.buyer:
            raise PermissionDenied()
        if not obj.debt:
            raise PermissionDenied()
        return False

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        self._check_payment_not_possible(obj)
        return obj

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context['form'] = SeasonTicketOrderForm(
            initial={'comment': self.object.comment})
        return self.render_to_response(context)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = SeasonTicketOrderForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            resp, error = self.paid_season_ticket(request, self.get_object(),
                                                  data['comment'])

            redirect_url = resp.get('formUrl') or reverse_lazy('private:index')

            if error:
                messages.add_message(request, messages.WARNING,
                                     'Ошибка при создании оплаты')

            return HttpResponseRedirect(redirect_url)

        return self.render_to_response({})
